import {Button, ButtonGroup} from "@material-ui/core";
import Link from "next/link";
import React from "react";

export default function Nav() {
    return (
        <nav>
            <ButtonGroup>
                <Link href={"/add-note"}><Button>Add</Button></Link>
            </ButtonGroup>
        </nav>
    )
}