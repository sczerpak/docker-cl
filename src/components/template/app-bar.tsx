import React from 'react';
import {AppBar as AppBarMUI, Container, Grid} from "@material-ui/core";
import {Logo} from "./logo";
import Nav from "./nav";



const AppBar = () => {

    return (
        <AppBarMUI>
            <Container>
                <Grid container
                      direction="row"
                      justify="space-between"
                      alignItems="center">

                    <Logo/>
                    <Nav/>
                </Grid>
            </Container>
        </AppBarMUI>
    );
};

export default AppBar;