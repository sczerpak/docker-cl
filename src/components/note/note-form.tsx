import React from 'react';
import {Button, Container, FormControl, Icon, Paper, Typography} from "@material-ui/core";
import {Field, Form, Formik} from "formik";
import {Note} from "../../models/note";
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import SaveIcon from '@material-ui/icons/Save';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        margin: {
            margin: theme.spacing(1),
        }
    }),
);


export default function NoteForm() {
    const classes = useStyles();

    return (
        <Paper style={{padding:"20px"}}>

        <Typography variant={"h4"}>New note</Typography>
            <Formik
                initialValues={new Note()}
                onSubmit={(values, formik) => {
                    formik.setSubmitting(false);
                    console.log(values);
                }}
            >
                <Form>
                    <FormControl fullWidth>
                        <Field type={"text"}
                               name={"title"}
                               as={TextField}
                               label={"Title"}
                               className={classes.margin}
                        />

                        <Field type={"textarea"}
                               name={"content"}
                               as={TextField}
                               className={classes.margin}
                               label={"Content"}
                               multiline
                               rows={8}
                        />
                        <Button type={"submit"}
                                variant="contained"
                                color="primary"
                                endIcon={<SaveIcon/>}
                        >
                            Save
                        </Button>
                    </FormControl>
                </Form>
            </Formik>
        </Paper>
    );
}
