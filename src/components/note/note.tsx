import React from 'react';
import {Note as NoteModel} from "../../models/note";
import {Button, Card, Container, Grid, Paper, Typography} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";

const Note = ({note}: { note: NoteModel }) => {
    return (
        <Paper style={{padding: "20px", margin: "20px 0"}}>
            <Grid container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                  spacing={1}
            >
                <Grid item xs={9}>
                    <Typography variant={"h6"}>{note.title}</Typography>
                    <p>{note.content}</p>
                </Grid>
                <Grid item xs={2}>
                    <Button> <EditIcon /></Button>
                    <Button> <DeleteIcon /></Button>
                </Grid>
            </Grid>
        </Paper>
    )
}

export default Note;