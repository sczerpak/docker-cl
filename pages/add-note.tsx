import Template from "../src/components/template/template";
import NoteForm from "../src/components/note/note-form";

export default function AddNote() {
    return (
        <Template>
            <NoteForm/>
        </Template>
    )
}
