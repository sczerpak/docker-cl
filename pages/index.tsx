import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Template from "../src/components/template/template";
import NoteForm from "../src/components/note/note-form";
import {Typography} from "@material-ui/core";
import NoteList from "../src/components/note/note-list";

export default function Home() {
    return (
        <Template>
            <Typography variant={"h4"}>Notes</Typography>
            <NoteList/>
        </Template>
    )
}
