This is a  [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app) 
created for demonstration / learning Next.js purposes.

You can find video materials on YT channel: [https://youtube.com/DariuszLuber](https://youtube.com/DariuszLuber) 

## Getting Started

Install Node.js and then 

## For development purposes

```bash
npm i
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## For production purposes

```bash
npm i
npm run build
npm run start
```